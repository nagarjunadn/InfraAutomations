# Lambda IAM Role Policy

{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": "arn:aws:logs:*:*:*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "ec2:Describe*",
                "ec2:StartInstances",
                "ec2:StopInstances"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "redshift:CreateClusterSnapshot",
                "redshift:CreateCluster",
                "redshift:DeleteCluster",
                "redshift:DeleteClusterSnapshot",
	"redshift:RestoreFromClusterSnapshot",
                "redshift:Describe*"
            ],
            "Resource": "*"
        }
    ]
}
