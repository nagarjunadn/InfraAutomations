import boto3
import datetime

ec2_client = boto3.client('ec2', 'ap-south-1')
redshift_client = boto3.client('redshift','ap-south-1')

# Enter the instances to start: ex. ['X-XXXXXXXX', 'X-XXXXXXXX']
instances = ['i-0f7316XXX']

#Latest snapshot date to be considered
TODAY = datetime.datetime.now()
LAST_DAY=  TODAY - datetime.timedelta(days=0) 
LATEST_SNAP_DATE=LAST_DAY.strftime('%d-%m-%Y')

#Enter the redshift cluster name and the latest snapshot to restore from
CLUSTER_NAME="lambdatest"
LATEST_SNAPSHOT_NAME=CLUSTER_NAME+"-snap-"+LATEST_SNAP_DATE

def lambda_handler(event, context):

# starting up EC2...
	try:
		ec2_start=ec2_client.start_instances(
		    InstanceIds=instances
		)
		print 'started instance(s): ' + str(instances)

	except Exception as e:
         print "%s" % e.message
		 
# Starting up redshift...
    #aws redshift restore-from-cluster-snapshot --cluster-identifier mycluster --snapshot-identifier my-snapshot-id
	try:
		restored_cluster = redshift_client.restore_from_cluster_snapshot(
			ClusterIdentifier=CLUSTER_NAME,
			SnapshotIdentifier=LATEST_SNAPSHOT_NAME,
		)
		print " \"%s\" Cluster is restored from the latest snapshot \"%s\"" %(CLUSTER_NAME,LATEST_SNAPSHOT_NAME)
	except Exception as e:
         print "%s" % e.message
