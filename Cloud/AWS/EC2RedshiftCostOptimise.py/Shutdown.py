import boto3
import datetime

ec2_client = boto3.client('ec2','ap-south-1')
redshift_client = boto3.client('redshift','ap-south-1')

# Enter your instances here: ex. ['X-XXXXXXXX', 'X-XXXXXXXX']
instances = ['i-0f73XXXXX']

##Enter your redshift cluster name
CLUSTER_NAME="lambdatest"

#Append with current date to create redshift snapshot
TODAY = datetime.datetime.now()
CURRENT_DATE = TODAY.strftime('%d-%m-%Y')
NEW_SNAPSHOT_NAME=CLUSTER_NAME+"-snap-"+CURRENT_DATE

#days old to delete
DELETE_DAY=  TODAY - datetime.timedelta(days=4) 
DELETE_DATE=DELETE_DAY.strftime('%d-%m-%Y')
        
OLD_SNAPSHOT_DELETE=CLUSTER_NAME+"-snap-"+DELETE_DATE

def lambda_handler(event, context):

# shutting down EC2...
	try:
		ec2_stop=ec2_client.stop_instances(
		InstanceIds=instances
		)
		print 'stopped instance(s): ' + str(instances)
	
	except Exception as e:
	    print "%s" % e.message
	    
# shutting down redshift...
    #aws redshift delete-cluster --cluster-identifier mycluster --final-cluster-snapshot-identifier my-snapshot-id
	try:
	   	delete_redshift_take_snapshot = redshift_client.delete_cluster(
			ClusterIdentifier=CLUSTER_NAME,
			SkipFinalClusterSnapshot=False,
			FinalClusterSnapshotIdentifier=NEW_SNAPSHOT_NAME
		)
		print "\"%s\" cluster is deleted by taking the new snapshot \"%s\"" %(CLUSTER_NAME,NEW_SNAPSHOT_NAME)
	
	except Exception as e:
	    print "%s" % e.message
	    
	try:
	##delete older than some days
	    delete_old_redshift_snapshot = redshift_client.delete_cluster_snapshot(
	        SnapshotIdentifier=OLD_SNAPSHOT_DELETE,
	        SnapshotClusterIdentifier=CLUSTER_NAME
    	)
	    print " \"%s\" snapshot is deleted for the cluster \"%s\"" %(OLD_SNAPSHOT_DELETE,CLUSTER_NAME)
	    
	except Exception as e:
    print "%s" % e.message