# Lambda IAM Role Policy:
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": "arn:aws:logs:*:*:*"
        },
        {
            "Action": [
                "elasticache:Describe*",
                "elasticache:CopySnapshot",
                "elasticache:AddTagsToResource",
                "elasticache:CreateSnapshot",
                "elasticache:DeleteSnapshot",
                "elasticache:ListTagsForResource"
            ],
            "Effect": "Allow",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "s3:ListAllMyBuckets",
            "Resource": "arn:aws:s3:::*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:Get*",
                "s3:List*",
                "S3:Put*"
            ],
            "Resource": [
                "arn:aws:s3:::ec-backup-mumbai",
                "arn:aws:s3:::ec-backup-mumbai/*"
            ]
        }
    ]
}
