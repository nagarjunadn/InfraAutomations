import boto3
import botocore
import datetime
import time

#Source region where the ElastiCache cluster resides
SOURCE_REGION = 'ap-south-1'
date = datetime.datetime.now()
#create_fmt = date.strftime('-%Y-%m-%d')
create_fmt = date.strftime('-%d-%m-%Y')

client = boto3.client('elasticache', region_name=SOURCE_REGION)
cc=client.describe_cache_clusters()

def lambda_handler(event, context):
  for cluster in cc.get('CacheClusters'):
      try:
        print (cluster['CacheClusterId']);
        print (cluster['CacheClusterId']+create_fmt)
        response = client.create_snapshot(
            CacheClusterId= cluster['CacheClusterId'] ,
            SnapshotName= cluster['CacheClusterId']+create_fmt
        )
      except Exception as e:      
             print "%s" % e.message