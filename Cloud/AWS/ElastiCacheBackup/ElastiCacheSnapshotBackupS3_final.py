#This script copies the snapshots which are in available state
#Note this script works only if the number of cacheclusters are upto maximum 100. And copies the snapshots created for the same day

import boto3
import botocore
from datetime import timedelta
import datetime

SOURCE_REGION = 'ap-south-1'
client = boto3.client('elasticache',region_name=SOURCE_REGION)

s3bucket = 'ec-backup-mumbai'

#To sort out the snapshots date matches todays date to copy
today = datetime.datetime.now()
#today_date_fmt = today.strftime('%Y-%m-%d')
today_date_fmt = today.strftime('%d-%m-%Y')
no_of_days_old = today - datetime.timedelta(days=0) # Zero means todays date
#old_date_fmt = no_of_days_old.strftime('%Y-%m-%d')
old_date_fmt = no_of_days_old.strftime('%d-%m-%Y')

#To sort out the snapshots date matches the old date to delete
delete_older_days = today - datetime.timedelta(days=2) # Zero means todays date and 2 means two days old 
#delete_old_date_fmt = delete_older_days.strftime('%Y-%m-%d')
delete_old_date_fmt = delete_older_days.strftime('%d-%m-%Y')
        
def lambda_handler(event, context):
    #print (old_date_fmt) 
    #print (today_date_fmt)
    
    cluster_instances = client.describe_cache_clusters(ShowCacheNodeInfo=True)
    #print (cluster_instances['CacheClusters'][1]['CacheClusterId'])
    try:
     for cluster_instance in cluster_instances['CacheClusters']:
        #print (cluster_instance['CacheClusterId'])
        cluster_Snaps = client.describe_snapshots(CacheClusterId=cluster_instance['CacheClusterId'], SnapshotSource='user')
        #print (cluster_Snaps['Snapshots'])
        
        for cluster_snap in cluster_Snaps['Snapshots']:
            snapshotname = cluster_snap['SnapshotName']
            #print (snapshotname)
            try:
                if (snapshotname.find(today_date_fmt)>=0 and old_date_fmt==today_date_fmt): 
                    response = client.copy_snapshot( 
                        SourceSnapshotName=snapshotname,
                        TargetSnapshotName=snapshotname,
                        TargetBucket=s3bucket
                    )
                    print("Shanpshot \"%s\" copied to s3 bucket which matches the date" % snapshotname)
                #else:
                 #   print ("Shanpshot \"%s\" not copied to s3 bucket which doesn't matches the date" % snapshotname)
            except Exception as e:      
                   print "%s" % e.message 


        print ("delete date %s" %delete_old_date_fmt)
        for cluster_snap in cluster_Snaps['Snapshots']:
            snapshotname = cluster_snap['SnapshotName']
            #print (snapshotname)
            try:
                if (snapshotname.find(delete_old_date_fmt)>=0):
                    print ("To be deleted snapshot is: %s" %snapshotname)
                    response = client.delete_snapshot(
						SnapshotName=snapshotname
					)

                    print("Shanpshot \"%s\" deleted which matches the date" % snapshotname)
                #else:
                 #   print ("Shanpshot \"%s\" not deleted which doesn't matches the date" % snapshotname)
            except Exception as e:      
                   print "%s" % e.message 
				   
    except Exception as e:      
                   print "%s" % e.message          