import boto3
import botocore
import datetime


bucket = 'testbucket'

client = boto3.client('elasticache')
response1 = client.describe_cache_clusters()

def lambda_handler(event, context):
    for cluster in response1.get('CacheClusters'):
      print (cluster['CacheSubnetGroupName']);
      response = client.create_cache_cluster(
			CacheClusterId='test-redis-new',
			AZMode='single-az',
			PreferredAvailabilityZone='ap-southeast-1a',
			CacheNodeType='cache.t2.small',
			NumCacheNodes=1,
			Engine='Redis',
			EngineVersion='3.2.4',
			CacheParameterGroupName='default.redis3.2',
			CacheSubnetGroupName= cluster['CacheSubnetGroupName'] ,
			SnapshotArns=['arn:aws:s3:::bucket/CacheClusterId*'],
			Port=6379
	)
    