import boto3
import collections
import datetime
import time 
import re

#Global objects 
ec = boto3.resource('ec2', 'ap-southeast-1')
images = ec.images.filter(Owners=["XXXXX"])# Specify your AWS account owner id in place of "XXXXX" at all the places in this script
autoscale = boto3.client('autoscaling', 'ap-southeast-1')


##To sort out the ami's date 
today = datetime.datetime.now()
#To sort out the ami's that matches the date to be updated in the new launch configuration
days_old = today - datetime.timedelta(days=1) # Zero means todays date and 1 means yesterday  
days_old_date = days_old.strftime('%d-%m-%Y')

#Keep adding more number of AMI prefix names created from lambda here
apache_app = 'Lambda - '+'Apache-app' # 'Apache-app' can be the prefix name for ASG Ex: Apache-app-DR-ASG
tomcat_app = 'Lambda - '+'Tomcat-app'

#The variable should be comma separated
image_names_array = [apache_app,tomcat_app]

#To update in Auto-scaling, You can change to keep min instances during DR. 
MIN_SIZE=1
DESIRED_CAPACITY=1
MAX_SIZE=2

def lambda_handler(event, context):
    imagesList = []
    
    for image_name in image_names_array:    
        for image in images:
            if ((image.name.startswith(image_name)) and (image.name.endswith(days_old_date))):    
                imagesList.append(image.name)
                print  ("\nThe new AMI to be used with new LC is: \"%s\"" %image.name)
                
                #Existing Autoscaling group details, ASG be must be manually created.
                #lambda_prefix_asg_name = trunc_at(image.name, "-",2) # Ex: Lambda - Apache
                lambda_prefix_asg_name = trunc_at(image.name, "-",3) # Ex: Lambda - Apache-app
                ASG_NAME = lambda_prefix_asg_name.replace('Lambda - ', '')+'-DR-ASG'
                    
                try:
                     update_autoscale_group = autoscale.update_auto_scaling_group(
                          AutoScalingGroupName=ASG_NAME,
                          MinSize=int(MIN_SIZE), 
                          DesiredCapacity=int(DESIRED_CAPACITY),# remove 1 instance
                          MaxSize=int(MAX_SIZE)
                        )
                     print ("The ASG %s updated with MIN_SIZE:%d, DESIRED_CAPACITY:%d, MAX_SIZE:%d " %(ASG_NAME,MIN_SIZE,
                     DESIRED_CAPACITY,MAX_SIZE))

                        
                except Exception as e:
                      print "%s" % e.message
					  
def trunc_at(s, d, n=3):
    "Returns s truncated at the n'th (3rd by default) occurrence of the delimiter, d."
    return d.join(s.split(d)[:n])