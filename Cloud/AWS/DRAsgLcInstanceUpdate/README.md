# Prerequisite

�	Assuming you have the required setup has been configured with the auto scaling group for the application type.

�	AMI must have created and copied to DR region. To do that refer. 

	1. https://gitlab.com/nagarjunadn/InfraAutomations/blob/master/Cloud/AWS/AMIBackupCleanup/

	2. https://gitlab.com/nagarjunadn/InfraAutomations/tree/master/Cloud/AWS/AMICopy/
	
�	The auto scaling group names should be same as app ec2 instance name and are case sensitive    'CHARS'-'CHARS'-DR-ASG.
Example: Apache-app-DR-ASG


# DRInstanceUpdate 

�	Checks the latest AMI exists in the DR region and store the AMI id in new variable.

�	Describes the auto scaling group by matching the truncated string of the AMI name and get the current LC details into variables.

�	Creates a new launch configuration with the date with all old options and only update new AMI id in it.

�	Updates the auto scaling group with the new Launch Configuration name and increase min instances to 2 which in turn launch the new instance. Now all the new instances launched by the Auto Scaling will be according to the new launch configuration.

�	Deletes the old launch configuration.

# DRInstanceOldDelete 

�	Checks the latest AMI exists in DR and store the AMI id in the new variable.

�	Gets the auto scaling group group by matching the truncated string of the AMI name and updates auto scaling group by decreasing min instances to 1 which deletes the instance which was launched from old launch configurations.



# Important Note:
 Please specify your AWS Account Number in the place of "XXXXX" where ever in the code