import boto3

#Global objects 
autoscale = boto3.client('autoscaling', 'ap-south-1')

#Auto-scaling group names list which are in comma seperated
asg_names_array = ['test-asg1','test-asg1']

#Alter the min size as needed
min_size = 4

def lambda_handler(event, context):
    
    
    for ASG_NAME in asg_names_array:    
        try:    
            update_autoscale_group = autoscale.update_auto_scaling_group(
                            AutoScalingGroupName=ASG_NAME,
                            MinSize=int(min_size) # Adds up the instances
                            
                        )
            print ("The ASG \'%s\' has been successfully updated with the Min Size: %s" %(ASG_NAME,int(min_size)) )

        except Exception as e:
            print ("ASG \'%s\' doesn't exist or unnexpected error: %s" %(ASG_NAME,e.message))
